class CreateCarts < ActiveRecord::Migration[6.0]
  def change
    create_table :carts do |t|
      t.string :item
      t.integer :quantity
      t.float :cart_value
      t.references :customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
