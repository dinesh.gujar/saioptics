class PagesController < ApplicationController
  def index
    @spects = Spectacle.all.order(created_at: :desc)
  end
end
