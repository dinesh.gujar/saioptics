class CreateLenses < ActiveRecord::Migration[6.0]
  def change
    create_table :lenses do |t|
      t.references :spectacle, null: false, foreign_key: true
      t.string :cylindrical
      t.string :spherical
      t.string :axis
      t.float  :power
      t.string :company
      t.string :type
      t.float  :price
      t.timestamps
    end
  end
end
