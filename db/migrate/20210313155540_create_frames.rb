class CreateFrames < ActiveRecord::Migration[6.0]
  def change
    create_table :frames do |t|
      t.references :spectacle, null: false, foreign_key: true
      t.string :type
      t.string :company
      t.float :price
      t.timestamps
    end
  end
end
