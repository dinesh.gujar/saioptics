class RenameTypeColumnOfLenseAndFrame < ActiveRecord::Migration[6.0]
  def up
    rename_column :lenses, :type, :lense_type
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    rename_column :lenses, :price, :lense_price
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    rename_column :frames, :type, :frame_type
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    rename_column :frames, :price, :frame_price
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
  end

  def down
    rename_column :lenses, :lense_type, :type
    rename_column :lenses, :lense_price, :price
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    rename_column :frames, :frame_type, :type
    rename_column :frames, :frame_price, :price
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
  end
end
