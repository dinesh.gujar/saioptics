require 'rails_helper'

RSpec.describe "carts/index", type: :view do
  before(:each) do
    assign(:carts, [
      Cart.create!(
        item: "Item",
        quantity: 2,
        cart_value: 3.5,
        customer: nil
      ),
      Cart.create!(
        item: "Item",
        quantity: 2,
        cart_value: 3.5,
        customer: nil
      )
    ])
  end

  it "renders a list of carts" do
    render
    assert_select "tr>td", text: "Item".to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.5.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end
