class ChangeMobileNoCustomers < ActiveRecord::Migration[6.0]
  def change
    change_column :customers, :mobile_no, :string, :limit => 13
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
