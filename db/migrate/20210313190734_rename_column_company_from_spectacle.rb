class RenameColumnCompanyFromSpectacle < ActiveRecord::Migration[6.0]
  def up
    rename_column :spectacles, :Company, :company
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    rename_column :spectacles, :Type, :stype
  end

  def down
    rename_column :spectacles, :company, :Company
    rename_column :spectacles, :stype, :Type
  end
end
