Rails.application.routes.draw do
  resources :carts
  devise_for :users
  resources :customers
  resources :spectacles
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #root "customers#index"
  root "pages#index"
end
