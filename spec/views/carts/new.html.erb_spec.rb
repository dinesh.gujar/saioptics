require 'rails_helper'

RSpec.describe "carts/new", type: :view do
  before(:each) do
    assign(:cart, Cart.new(
      item: "MyString",
      quantity: 1,
      cart_value: 1.5,
      customer: nil
    ))
  end

  it "renders new cart form" do
    render

    assert_select "form[action=?][method=?]", carts_path, "post" do

      assert_select "input[name=?]", "cart[item]"

      assert_select "input[name=?]", "cart[quantity]"

      assert_select "input[name=?]", "cart[cart_value]"

      assert_select "input[name=?]", "cart[customer_id]"
    end
  end
end
