class AddPurchasedAtToCustomers < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :purchased_at, :date
  end
end
