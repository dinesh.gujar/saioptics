require 'rails_helper'

RSpec.describe "carts/edit", type: :view do
  before(:each) do
    @cart = assign(:cart, Cart.create!(
      item: "MyString",
      quantity: 1,
      cart_value: 1.5,
      customer: nil
    ))
  end

  it "renders the edit cart form" do
    render

    assert_select "form[action=?][method=?]", cart_path(@cart), "post" do

      assert_select "input[name=?]", "cart[item]"

      assert_select "input[name=?]", "cart[quantity]"

      assert_select "input[name=?]", "cart[cart_value]"

      assert_select "input[name=?]", "cart[customer_id]"
    end
  end
end
