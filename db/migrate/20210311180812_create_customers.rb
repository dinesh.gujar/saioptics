class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.text :address
      t.integer :mobile_no
      t.float :left_glass_power, default: 0.25
      t.float :right_glass_power, default: 0.25  
      t.string :glass_company
      t.string :frame_company
      t.string :frame_type
      t.float :price
      t.string :email

      t.timestamps
    end
  end
end
