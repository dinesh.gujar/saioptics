class Spectacle < ApplicationRecord
  has_many :lenses, dependent: :destroy
  has_many :frames, dependent: :destroy
end
