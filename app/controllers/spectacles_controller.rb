class SpectaclesController < ApplicationController
  before_action :set_spectacle, only: %i[show edit update destroy]

  def index
    @spects = Spectacle.all
  end

  def new
    @spect = Spectacle.new
  end

  def show
  end

  def create
    @spect = Spectacle.create(company: spects_params[:company], stype: spects_params[:stype])
    if @spect.save
      if Frame.create(spectacle_id: @spect.id, frame_type: spects_params[:frame_type], frame_price: spects_params[:frame_price]) and Lense.create(spectacle_id: @spect.id, lense_price: spects_params[:lense_price], lense_type: spects_params[:lense_type])
        redirect_to spectacles_path, notice: "Success"
      else
        redirect_to spectacles_path, notice: " Not success"
      end
    end
  end

  def destroy
    #@spect = Spectacle.find(params[:id])
    @spect.destroy
    redirect_to spectacles_path, notice: "Success"

    #redirect_to spectacles_path, notice: "Not Success"
  end

  def update
    if @spect.update(spects_params)
      redirect_to @spect, notice: "Success"
    else
      redirect_to @spect, notice: "Success"
    end
  end

  private

  def set_spectacle
    @spect = Spectacle.find(params[:id])
  end

  def spects_params
    params.require(:spectacle).permit(:company, :stype, :frame_type, :frame_price, :cylindrical, :spherical, :axis, :power, :lense_type, :lense_price)
  end
end
