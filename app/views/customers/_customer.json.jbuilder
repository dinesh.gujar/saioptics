json.extract! customer, :id, :name, :address, :mobile_no, :left_glass_power, :right_glass_power, :glass_company, :frame_company, :frame_type, :price, :email, :created_at, :updated_at
json.url customer_url(customer, format: :json)
