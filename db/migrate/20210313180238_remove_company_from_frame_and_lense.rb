class RemoveCompanyFromFrameAndLense < ActiveRecord::Migration[6.0]
  def up
    remove_column :frames, :company
    remove_column :lenses, :company
  end

  def down
    add_column :frames, :company
    add_column :lenses, :company
  end
end
