FactoryBot.define do
  factory :cart do
    item { "MyString" }
    quantity { 1 }
    cart_value { 1.5 }
    customer { nil }
  end
end
