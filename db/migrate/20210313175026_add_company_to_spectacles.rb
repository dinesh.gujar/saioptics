class AddCompanyToSpectacles < ActiveRecord::Migration[6.0]
  def up
    add_column :spectacles, :Company, :string
    add_column :spectacles, :Type, :string
  end

  def down
    remove_column :spectacles, :Company, :string
    remove_column :spectacles, :Type, :string
  end
end
