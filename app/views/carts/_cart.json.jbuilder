json.extract! cart, :id, :item, :quantity, :cart_value, :customer_id, :created_at, :updated_at
json.url cart_url(cart, format: :json)
